<?php get_header(); ?>

  <div class="container-fluid" data-bs-parallax-bg="true" id="home">
        <div class="row">
            <div class="col-10 col-lg-10 offset-1 offset-lg-1 pulse animated">
                <h1>Websites intuitivos, desenvolvidos por uma equipe de profissionais</h1>
                <p>Fuja dos layouts bagunçados, da falta de hierarquia visual ou da sobreposição de elementos. Esses erros prejudicam a experiência do usuário e por consequência a sua empresa.<br></p><a href="#bloco-produtos" class="btn-info">Saiba mais</a></div>
        </div>
    </div>
    <div class="container" id="mobile-first">
        <div class="row" data-aos="fade-up">
            <div class="col-lg-6 offset-lg-3">
                <h2 class="text-center">Do menor para o maior</h2>
                <p class="text-center">Projetamos sites otimizados, visando a navegabilidade e performance</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-8"><img class="img-fluid" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/app-screen.png" data-aos="fade-left" data-aos-delay="300"></div>
            <div class="col-md-12 col-lg-4" data-aos="fade-right" data-aos-delay="300">
                <div class="float-left margem-t-40 box-icon-mobile"><img class="d-inline float-left" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-graph-1.svg" alt="91%" width="70px">
                    <p class="d-inline float-left tamanho-paragrafo">da população mundial tem celular</p>
                </div>
                <div class="float-left margem-t-40 box-icon-mobile"><img class="d-inline float-left" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-graph-2.svg" alt="91%" width="70px">
                    <p class="d-inline float-left tamanho-paragrafo">possuem um smartphone</p>
                </div>
                <div class="float-left margem-t-40 box-icon-mobile"><img class="d-inline float-left" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-graph-3.svg" alt="91%" width="70px">
                    <p class="d-inline float-left tamanho-paragrafo">usam o celular como principal fonte de acesso<br></p>
                </div>
                <div class="float-left margem-t-40 box-icon-mobile"><img class="d-inline float-left" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-graph-4.svg" alt="91%" width="70px">
                    <p class="d-inline float-left tamanho-paragrafo">dos brasileiros não saem de casa sem o celular<br></p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" data-aos="fade-up" id="site-em-4-etapas">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <h2 class="text-center">Seu site em 4 etapas</h2>
                <p class="text-center">Processos bem definidos agilizam a entrega e garantem a qualidade que sua empresa merece&nbsp;<br></p>
            </div>
        </div>
        <div class="row margem-t-10">
            <div class="col-12 col-md-2 col-lg-2 offset-md-2 offset-lg-2 etapas-icones" data-bs-hover-animate="pulse"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-briefing.svg" alt="Briefing" width="120px" height="120px" style="width:90px;height:90px;">
                <p class="text-center">Briefing +</p>
            </div>
            <div class="col-12 col-md-2 col-lg-2 etapas-icones" data-bs-hover-animate="pulse"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-design.svg" alt="Briefing" width="120px" height="120px" style="width:90px;height:90px;">
                <p class="text-center">Design +</p>
            </div>
            <div class="col-12 col-md-2 col-lg-2 etapas-icones" data-bs-hover-animate="pulse"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-code.svg" alt="Briefing" width="120px" height="120px" style="width:90px;height:90px;">
                <p class="text-center">Desenvolvimento +</p>
            </div>
            <div class="col-12 col-md-2 col-lg-2 etapas-icones" data-bs-hover-animate="pulse"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-pc.svg" alt="Briefing" width="120px" height="120px" style="width:90px;height:90px;">
                <p class="text-center">Entrega +</p>
            </div>
        </div>
    </div>
    <div class="container-fluid beneficios" data-aos="fade-up" data-aos-delay="200">
        <div class="row">
            <div class="col-lg-5 offset-lg-1">
                <h2>Sites otimizados para mecanismos de busca (SEO)</h2>
                <div class="lista-beneficio"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-prog.svg">
                    <p>Desenvolvido em HTML, CSS e Javascript/Jquery</p>
                </div>
                <div class="lista-beneficio"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-pencil.svg">
                    <p>Ambiente administrativo em Wordpress</p>
                </div>
                <div class="lista-beneficio"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-mobile.svg">
                    <p>100% Responsivo. Seu site bonito e funcional em qualquer dispositivo móvel</p>
                </div>
                <div class="lista-beneficio"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/icon-mobile_2.svg">
                    <p>Hospedagem inclusa em todos os pacotes</p>
                </div>
            </div>
            <div class="col-lg-5" id="bloco-produtos">
                <div class="produto">
                    <div class="descritivo">
                        <h3>Blog</h3>
                        <p>A partir 12x de R$ 199,00<br></p>
                        <p class="linha-fina">Desconto de 5% a vista</p>
                    </div>
                    <div class="btn-info"><a href="#modal-blog" data-toggle="modal">Quero Contratar</a></div>
                </div>
                <div class="produto">
                    <div class="descritivo">
                        <h3>Website</h3>
                        <p>A partir de 12x de R$ 399,00<br></p>
                        <p class="linha-fina">Desconto de 5% a vista</p>
                    </div>
                    <div class="btn-info"><a href="#modal-website" data-toggle="modal">Quero Contratar</a></div>
                </div>
                <div class="produto">
                    <div class="descritivo">
                        <h3>Temos diversos materiais Digitais</h3>
                        <!-- <p>A partir de 12x <br></p> -->
                        <p class="linha-fina">E-mail marketing, landing pages, posts, apresentações e mais</p>
                    </div>
                    <div class="btn-info"><a href="#modal-website-blog" data-toggle="modal">Quero Contratar</a></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal blog -->
    <div class="modal fade" id="modal-blog" tabindex="-1" role="dialog" aria-labelledby="modal-blogLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-blog-titulo">Preencha o formulário abaixo e entraremos em contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div role="main" id="webspresso-blog-514ee855dd101bc536f3"></div> 
                <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script> 
                <script type="text/javascript">
                    new RDStationForms('webspresso-blog-514ee855dd101bc536f3-html', 'UA-70037167-1').createForm();
                </script>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
            </div>
            </div>
        </div>
    </div>

    <!-- Modal website -->
    <div class="modal fade" id="modal-website" tabindex="-1" role="dialog" aria-labelledby="modal-websiteLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-website-titulo">Preencha o formulário abaixo e entraremos em contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div role="main" id="webspresso-4069b23d51b771751da9"></div> 
                    <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script> 
                    <script type="text/javascript">
                        new RDStationForms('webspresso-4069b23d51b771751da9-html', 'UA-70037167-1').createForm();
                    </script>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
            </div>
            </div>
        </div>
    </div>

    <!-- Modal website + blog -->
    <div class="modal fade" id="modal-website-blog" tabindex="-1" role="dialog" aria-labelledby="modal-website-blogLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-website-blog-titulo">Preencha o formulário abaixo e entraremos em contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div role="main" id="webspresso-posts-060da9372a185062f0a7"></div> 
                <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script> 
                <script type="text/javascript">
                    new RDStationForms('webspresso-posts-060da9372a185062f0a7-html', 'UA-70037167-1').createForm();
                </script>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
            </div>
            </div>
        </div>
    </div>


     <!-- Modal ---------- Botao especialista ------------ -->
     <div class="modal fade" id="modal-especialista" tabindex="-1" role="dialog" aria-labelledby="modal-especialistaLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-especialista-titulo">Preencha o formulário abaixo e entraremos em contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div role="main" id="webspresso-contato-28663cb18b3098a1fe8b"></div>
                <script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
                <script type="text/javascript">
                new RDStationForms('webspresso-contato-28663cb18b3098a1fe8b-html', 'UA-70037167-1').createForm();
                </script>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
            </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>