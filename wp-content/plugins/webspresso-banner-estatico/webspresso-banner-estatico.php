<?php 
/** 
* @package WebspressoBannerEstatico
*/
/*
Plugin Name: Webspresso Banner estatico com texto
Plugin URI: http://webspresso.com.br
Description: Plugin referente a banner fixo com texto e imagem de fundo
Version: 1.0.0
Author: Webspresso
Author URI: http://webspresso.com.br
Text Domain webspresso-banner-estatico
*/

// VERIFICA SE EXISTE PERMISSÃO PARA ACESSAR O ARQUIVO
defined( 'ABSPATH' ) or die('You do not have permission to access this file. ;-;');