
<?php get_header(); ?>
        <?php if(have_posts()) : ?>
            <?php while(have_posts()) : the_post() ?>

            <h3><?php the_content(); ?></h3>

            <?php endwhile; ?>

        <?php endif; ?> 

<?php get_footer(); ?>

