        <div data-aos="fade-up" data-aos-delay="300" class="footer-basic">
            <footer>
                <div class="social"><a href="#"><i class="icon ion-social-instagram"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-facebook"></i></a></div>
                <p class="copyright">webspresso © 2017 | Powered by Grano Studio</p>
            </footer>
        </div>

        <?php
            /* Always have wp_footer() just before the closing </body>
            * tag of your theme, or you will break many plugins, which
            * generally use this hook to reference JavaScript files.
            */
            wp_footer();
        ?>

    </body>
</html>