<?php 

/**
 * Trigger this file on plugin uninstall
 * @package WebspressoSlider
 * */

if( ! defined('WP_UNINSTALL_PLUGIN') ) {
    die;
}

// Clear database stored data

$slides = get_post( array('post_type' => 'slide', 'numberposts' => -1) );

foreach($slides as $slide) {
    wp_delete_post($slide->ID, true);
}