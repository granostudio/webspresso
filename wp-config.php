<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_webspresso');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']cF`HT&V *<LfI0]y4ml3ED~v1retLs&>~CEh)ik$ N8 v,fS5 S%:H&V`.=<p=j');
define('SECURE_AUTH_KEY',  '|a<jt;P6FA|jBz`#zf)J#+;ab;`R.!/XKlWw8&bID3ALO!61T$G7-]*xKY>36FV*');
define('LOGGED_IN_KEY',    '#}PGCZLw#L|ed`08uxa+>LI@x:XxVL5j<_pobyfs~06M|`IB^cCJC+sN4]|1|>s:');
define('NONCE_KEY',        'yisd<bYn!_dw}*Tvz9]nLSmyVnLgSk^ =lngY[]pQ7gPu.1J g[VAoHVQ><qDIVW');
define('AUTH_SALT',        'N,W#_tw@(aV<)EZ/#R{Z;/%m_?F!.|W[_]Ni1!G{<)_?Zn1AU3_*k6ioaXDQ9(c>');
define('SECURE_AUTH_SALT', '..2`x/OG*`h>F6=BoPPibv+rN6X+>/<N?@gh|2S?AZ#JrLK/6dm5w{5Sha#]dv>s');
define('LOGGED_IN_SALT',   'DdhLP5v9Qx[oI7:-}mX~<$Ya-Yd@By3h:n{&1]#%kPbu $zOPsZ,ZMGMvfpU?*)i');
define('NONCE_SALT',       'N|A$(9}QzNotT%fub}35YkgdwDZj!{4jfuIVD1Ug__v*]9&hq%`1U!G%+`w6J+9(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
