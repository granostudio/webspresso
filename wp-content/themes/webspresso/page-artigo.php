<?php /* Template Name: Pagina Artigos */ ?>

<?php get_header(); ?>
<?php 
    query_posts(array( 
        'post_type' => 'artigo',
        'showposts' => 10 
    ) );  
?>

<?php while (have_posts()) : the_post(); ?>
        <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
        <p><?php echo get_the_excerpt(); ?></p>
<?php endwhile;?>

<?php
$blogusers = get_users( 'orderby=nicename&role=articulista' );
// Array of WP_User objects.
foreach ( $blogusers as $user ) {
	echo '<span>' . $user->first_name . ' ' . $user->last_name . '</span>';
}
?>
<?php get_footer(); ?>

