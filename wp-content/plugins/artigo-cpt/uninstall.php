<?php 

/**
 * Trigger this file on plugin uninstall
 * @package ArtigoCpt
 * */

if( ! defined('WP_UNINSTALL_PLUGIN') ) {
    die;
}

// Clear database stored data

// $artigos = get_posts( array('the_post_type' => 'artigo', 'numberposts' => -1) );

// foreach ($artigos as $artigo) {
//     wp_delete_post($artigo->ID, true );
// }

// Access the database vai SQL
global $wpdb;

$wpdb->query( "DELETE FROM wp_posts WHERE post_type = 'artigo'"  );

$wpdb->query( "DELETE FROM wp_postmeta WHERE post_id NOT IN (SELECT id FROM wp_posts )" );
$wpdb->query( "DELETE FROM wp_term_relationships WHERE object_id NOT IN (SELECT id FROM wp_posts )" );

//check if role exist before removing it
if( get_role('articulista') ){
    remove_role( 'articulista' );
}
