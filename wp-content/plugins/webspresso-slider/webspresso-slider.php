<?php 
/** 
* @package WebspressoSlider
*/
/*
Plugin Name: Webspresso Slider plugin
Plugin URI: http://webspresso.com.br
Description: Plugin para slider
Version: 1.0.0
Author: Webspresso
Author URI: http://webspresso.com.br
Text Domain webspresso-slider
*/

// VERIFICA SE EXISTE PERMISSÃO PARA ACESSAR O ARQUIVO
defined( 'ABSPATH' ) or die('You do not have permission to access this file.');

class WebspressoPlugin
{
    function __construct() {
        add_action('init', array( $this,'webspresso_custom_post_type' ) );
    }
    function activate() {
        // generate cpt
        $this->webspresso_custom_post_type();
        // flush rewrite rules
        flush_rewrite_rules();
    }

    function deactivate() {
        // flush rewrite rules 
        flush_rewrite_rules();       
    }

    function webspresso_custom_post_type() {
        $args = array(
            'public'    => true,
            'label'     => __( 'Slides', 'textdomain' ),
            'menu_icon' => 'dashicons-image-flip-horizontal',
        );
        register_post_type( 'slide', $args );
    }
}

if (class_exists( 'WebspressoPlugin' ) ) {
    $webspresso_plugin = new WebspressoPlugin();
}

// activation
register_activation_hook(__FILE__, array($webspresso_plugin, 'activate'));

// deactivate
register_deactivation_hook(__FILE__, array($webspresso_plugin, 'deactivate'));

