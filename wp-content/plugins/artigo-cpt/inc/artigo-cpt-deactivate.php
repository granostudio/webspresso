<?php 
/**
 * @package ArtigoCpt
*/

class ArtigoPluginDeactivate 
{
    public static function deactivate() {
        flush_rewrite_rules();
    }
}