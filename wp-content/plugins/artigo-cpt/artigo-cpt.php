<?php
/**
 * @package ArtigoCpt
*/

/*
  Plugin Name: Plugin para Artigo CPT
  Description: Plugin referente a criação de area para Artigos
  Version: 1.0.0
  Author: Webspresso
  Text Domain: webspresso-plugin
*/

//* Evita o acesso direto ao arquivo
defined( 'ABSPATH' ) or die();

class ArtigoPlugin
{
  function __construct() {
    add_action ('init', array( $this, 'custom_post_type' ) );
    // add_action ('init', array( $this, 'create_articulista' ) );
    // add_action ('init', array( $this, 'add_admin_artigo_caps' ) );
    // add_action ('init', array( $this, 'add_articulista_artigo_caps' ) );
  }

  function activate() {
    // create cpt
    $this->custom_post_type();

    $this->create_articulista();

    $this->add_admin_artigo_caps();

    $this->add_articulista_artigo_caps();
    // rewrite rules
    flush_rewrite_rules();
  }

  function deactivate() {
    // rewrite rules
    flush_rewrite_rules();
  }

  function custom_post_type() {

    $artigo_singular = 'Artigo' ;
    $artigo_plural = 'Artigos' ;

    $labels = array (
      'name' => $artigo_plural,
      'singular_name' => $artigo_plural,
      'add_new' => 'Adicionar novo',
      'add_new_item' => 'Adicionar novo ' . $artigo_singular,
      'edit_item' => 'Editar ' . $artigo_singular,
      'new_item' => 'Novo ' . $artigo_singular,
      'view_item' => 'Visualizar ' . $artigo_singular,
      'view_items' => 'Visualizar ' . $artigo_plural,
      'search_items' => 'Pesquisar ' . $artigo_singular,
      'not_found' => 'Nenhum ' . $artigo_singular . ' encontrado',
      'not_found_in_trash' => 'Nenhum ' . $artigo_singular . ' encontrado no lixo',
      'all_items' => 'Todos os ' . $artigo_plural,
      'archives' => $artigo_singular . ' Archives',
      'attributes' => $artigo_singular . ' Atributos',
      'insert_into_item' => 'Inserir no ' . $artigo_singular,
      'uploaded_to_this_item' => 'Fazer upload para o ' . $artigo_singular,
      'featured_image' => 'Imagem destacada',
      'set_featured_image' => 'Definir imagem destacada',
      'remove_featured_image' => 'Remover imagem destacada',
      'use_featured_image' => 'Usar imagem destacada'

    );

    $sup = array(
      'title', 'author', 'editor', 'revisions', 'thumbnail'
    );

    $args = array(
      'labels' => $labels,
      'description' => 'CPT destinado a criação de artigos para articulistas',
      'public' => true,
      'menu_icon' => 'dashicons-products',
      'capabilities' => array(
        'edit_post' => 'edit_artigo',
        'edit_posts' => 'edit_artigos',
        'edit_others_posts' => 'edit_other_artigos',
        'publish_posts' => 'publish_artigos',
        'read_post' => 'read_artigo',
        'read_private_posts' => 'read_private_artigos',
        'delete_post' => 'delete_artigo',
      ),
      'supports' => $sup,
      // adding map_meta_cap will map the meta correctly 
    );

    register_post_type( 'artigo', $args );
  }

  function add_admin_artigo_caps() {
    // gets the administrator role
    $admins = get_role( 'administrator' );

    $admins->add_cap( 'edit_artigo' ); 
    $admins->add_cap( 'edit_artigos' ); 
    $admins->add_cap( 'edit_private_artigos' ); 
    $admins->add_cap( 'edit_published_artigos' ); 
    $admins->add_cap( 'edit_other_artigos' ); 
    $admins->add_cap( 'publish_artigos' ); 
    $admins->add_cap( 'read_artigo' ); 
    $admins->add_cap( 'read_private_artigos' ); 
    $admins->add_cap( 'delete_artigo' ); 
    $admins->add_cap( 'delete_others_artigos' ); 
    $admins->add_cap( 'delete_private_artigos' ); 
    $admins->add_cap( 'upload_files' ); 
  }

  function create_articulista() {
    add_role('articulista',
               'Articulista',
               array(
                   'read' => true,
                   'edit_posts' => true,
                   'delete_posts' => false,
                   'publish_posts' => true,
                   'upload_files' => true,
               )
           );
      }

      function add_articulista_artigo_caps() {
        // gets the administrator role
        $articulista = get_role( 'articulista' );
    
        $articulista->add_cap( 'edit_artigo' ); 
        $articulista->add_cap( 'edit_artigos' ); 
        $articulista->add_cap( 'edit_published_artigos' ); 
        $articulista->add_cap( 'publish_artigos' ); 
        $articulista->add_cap( 'read_artigo' ); 
        $articulista->add_cap( 'read_private_artigos' ); 
        $articulista->add_cap( 'delete_artigo' ); 
        $articulista->add_cap( 'upload_files' ); 
        $articulista->add_cap( 'create_artigos' ); 

      }


}


if( class_exists('ArtigoPlugin') ) {
  $artigoPlugin = new ArtigoPlugin();
}

//activation 
require_once plugin_dir_path(__FILE__) . 'inc/artigo-cpt-activate.php';
register_activation_hook( __FILE__, array('ArtigoPluginActivate', 'activate'));

// deactivation
require_once plugin_dir_path(__FILE__) . 'inc/artigo-cpt-deactivate.php';
register_deactivation_hook( __FILE__, array('ArtigoPluginDeactivate', 'deactivate'));

