<?php 

function webspresso_script_enqueue() {
    wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), '4.1.1', 'all' );
    wp_enqueue_style('webspresso-styles', get_template_directory_uri() . '/assets/css/styles.css', array(), '1.0.0', 'all' );
    wp_enqueue_style('webspresso-navigation-button', get_template_directory_uri() . '/assets/css/Navigation-with-Button.css', array(), '1.0.0', 'all' );
    wp_enqueue_style('webspresso-footer-basics', get_template_directory_uri() . '/assets/css/Footer-Basic.css', array(), '1.0.0', 'all' );
    wp_enqueue_style('webspresso-ionicons', get_template_directory_uri() . '/assets/fonts/ionicons.min.css', array(), '1.0.0', 'all' );
    wp_enqueue_style('google-font-ubuntu', 'https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,500,700', array(), 'all' );
    wp_enqueue_style('aos-animation-css', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css', array(), 'all' );
    wp_enqueue_style('animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css', array(), 'all' );

    wp_enqueue_script('webspresso-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '3.2.1', true );
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('webspresso-jquery'), '4.1.1', true );
    wp_enqueue_script('webspresso-bs-animation', get_template_directory_uri() . '/assets/js/bs-animation.js', array('webspresso-jquery'), true );    
    wp_enqueue_script('anos-animation-js', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js', array('webspresso-jquery'), true );
    wp_enqueue_script('scroll-js',  get_template_directory_uri() . '/assets/js/scroll.js', array('webspresso-jquery'), true );
}


add_action( 'wp_enqueue_scripts', 'webspresso_script_enqueue' );

// add menu option on admin

// function webspresso_theme_settings(){
//     add_theme_support('menus');
// }

// add_action('init', 'webspresso_theme_settings');
